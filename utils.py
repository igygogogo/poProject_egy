import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class Utils:
    __driver = None

    @classmethod
    def get_driver(cls):
        if cls.__driver is None:
            cls.__driver = webdriver.Chrome()
            cls.__driver.get("http://hmshop-test.itheima.net/Home/index/index.html")
            cls.__driver.maximize_window()

        return cls.__driver

    @classmethod
    def get_admin_driver(cls):
        if cls.__driver is None:
            cls.__driver = webdriver.Chrome()
            cls.__driver.get("http://hmshop-test.itheima.net/Admin/Admin/login")
            cls.__driver.maximize_window()

        return cls.__driver

    @classmethod
    def quit_driver(cls):
        if cls.__driver is not None:
            # 用这个也可以，可能还可以节省一些运行判断的时间
            cls.__driver.quit()
            # cls.get_driver().quit()
            cls.__driver = None


def get_msg():
    time.sleep(2)
    return Utils.get_driver().find_element(By.CLASS_NAME, "layui-layer-content").text

