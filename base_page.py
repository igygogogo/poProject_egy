from utils import Utils
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.select import Select

class BasePage:
    def __init__(self, x=None):
        # 通过super().__init__("admin")重写时输入的值，来判断要执行哪一个浏览器对象
        if x == "admin":
            self.driver = Utils.get_admin_driver()
        # 为什么这里不用webdriver去获取呢，因为在utils里面已经写了。
        # 同时基类的方法是获取元素对象和输入信息，针对的是同一个浏览器对象，那么既然已经有了，就直接使用就好了。
        else:
            self.driver = Utils.get_driver()

    def get_element(self, location):
        wait = WebDriverWait(self.driver, 10)
        return wait.until(lambda x: x.find_element(*location))

    def input_text(self, element, text):
        """
        :param element: 这里传入的是已经通过get_element获取的一个元素对象
        :param text:
        :return:
        """
        element.clear()
        element.send_keys(text)

    # 跳转framw页面
    def frame(self, element):
        self.driver.switch_to.frame(element)

    # 选项对象
    def create_selector(self, element, text):
        Select(element).select_by_visible_text(text)

