from selenium.webdriver.common.by import By

from base_page import BasePage


class LoginAdminPage(BasePage):
    def __init__(self):
        super().__init__("admin")
        self.username = By.NAME, "username"
        self.password = By.NAME, "password"
        self.code = By.ID, "vertify"
        self.login_btn = By.NAME, "submit"

    def login(self, user, pwd, cd):
        self.get_element(self.username).send_keys(user)
        self.get_element(self.password).send_keys(pwd)
        self.get_element(self.code).send_keys(cd)
        self.get_element(self.login_btn).click()


