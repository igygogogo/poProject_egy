import time

from selenium.webdriver.common.by import By

from base_page import BasePage


class BrandListPage(BasePage):
    def __init__(self):
        super().__init__("admin")
        self.frame_id = By.ID, "workspace"
        self.add_btn = By.CSS_SELECTOR, "[title='新增品牌']"
        self.brand_name = By.ID, "brand_name"
        self.selector1 = By.ID, "parent_id_1"
        self.selector2 = By.ID, "parent_id_2"
        self.btn = By.ID, "submitBtn"

        self.search_text = By.ID, "input-order-id"
        self.search_btn = By.XPATH, "//*[@value='搜索']"
        self.text = By.XPATH, "//*[@class='bDiv']/div/table/tbody/tr/td[3]/div"

        self.del_btn = By.XPATH, "//*[@class='btn red']"
        self.confirm_btn = By.CLASS_NAME, "layui-layer-btn0"

    # 跳转增加品牌页面
    def switch_frame(self):
        self.frame(self.get_element(self.frame_id))

    def go_to_add(self):
        self.get_element(self.add_btn).click()

    # 添加品牌
    def add_brand(self,name, select1, select2):
        """
        :param name:品牌名称
        :param select1: 品牌属性选择1：比如运动类
        :param select2: 品牌属性选择2：比如户外鞋服
        :return:
        """
        self.input_text(self.get_element(self.brand_name),name)
        self.create_selector(self.get_element(self.selector1), select1)
        time.sleep(1)
        self.create_selector(self.get_element(self.selector2), select2)
        self.get_element(self.btn).click()

    def search(self, text):
        self.input_text(self.get_element(self.search_text), text)
        self.get_element(self.search_btn).click()
        time.sleep(1)

    def get_text(self):
        return self.get_element(self.text).text

    def del_brand(self):
        self.get_element(self.del_btn).click()
        self.get_element(self.confirm_btn).click()
