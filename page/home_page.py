from base_page import BasePage
from selenium.webdriver.common.by import By


class HomePage(BasePage):
    def __init__(self):
        super().__init__()
        self.go_login = By.CLASS_NAME, "red"

    def go_to_login(self):
        self.get_element(self.go_login).click()
