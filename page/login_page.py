from base_page import BasePage
from selenium.webdriver.common.by import By


class LoginPage(BasePage):

    def __init__(self):
        super().__init__()
        self.username = By.ID, "username"
        self.password = By.ID, "password"
        self.code = By.ID, "verify_code"
        self.login_btn = By.CLASS_NAME, "J-login-submit"

    def login(self, user, pwd, cd):
        # 这里没有self.driver， get_element里面就有self.driver了
        self.input_text(self.get_element(self.username), user)
        self.input_text(self.get_element(self.password), pwd)
        self.input_text(self.get_element(self.code), cd)
        self.get_element(self.login_btn).click()
