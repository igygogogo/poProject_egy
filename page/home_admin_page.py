from selenium.webdriver.common.by import By

from base_page import BasePage


class HomeAdminPage(BasePage):
    def __init__(self):
        super().__init__("admin")
        self.shop = By.XPATH, "//*[@class='nc-module-menu']/ul/li[4]/a"
        self.brand_list = By.XPATH, "//*[text()='品牌列表']"

    def go_brand_list(self):
        self.get_element(self.shop).click()
        self.get_element(self.brand_list).click()
