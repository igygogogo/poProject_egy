import time

import pytest

from common.read_json_data import build_data
from config import BASE_DIR
from page.home_page import HomePage
from page.login_page import LoginPage
from utils import Utils, get_msg


class TestLogin:
    @classmethod
    def setup_class(cls):
        cls.home_proxy = HomePage()
        cls.login_proxy = LoginPage()
        cls.home_proxy.go_to_login()

    @classmethod
    def teardown_class(cls):
        time.sleep(3)
        Utils.quit_driver()

    @classmethod
    def setup(cls):
        Utils.get_driver().refresh()

    @pytest.mark.parametrize("username, password, code, res", build_data(BASE_DIR + '/data/login.json'))
    def test_login(self, username, password, code, res):
        self.login_proxy.login(username, password, code)
        assert res in get_msg()
