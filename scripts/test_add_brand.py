import time

from page.brand_list_page import BrandListPage
from page.home_admin_page import HomeAdminPage
from page.login_admin_page import LoginAdminPage
from utils import Utils


class TestLoginAdmin:
    @classmethod
    def setup_class(cls):
        cls.admin_login_proxy = LoginAdminPage()
        cls.home_admin_proxy = HomeAdminPage()
        cls.brand_list_proxy = BrandListPage()
        # 类前置只执行一次：后台登录
        cls.admin_login_proxy.login('admin', '123456', '8888')

    @classmethod
    def teardown_class(cls):
        time.sleep(3)
        Utils.quit_driver()

    @classmethod
    def setup(cls):
        Utils.get_driver().refresh()

    def test_01(self):
        # 进入品牌列表
        self.home_admin_proxy.go_brand_list()

        # # 切换frame页面，并且点击增加品牌按钮
        self.brand_list_proxy.switch_frame()
        self.brand_list_proxy.search("egy测试")
        msg = self.brand_list_proxy.get_text()
        if msg == "egy测试":
            self.brand_list_proxy.del_brand()

        self.brand_list_proxy.go_to_add()
        #
        self.brand_list_proxy.add_brand("egy测试", "运动户外", "户外鞋服")

        self.brand_list_proxy.search("egy测试")

        msg = self.brand_list_proxy.get_text()

        assert "egy测试" in msg


