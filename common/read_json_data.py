import json


def build_data(filepath):
    case_data = []
    with open(filepath, 'r', encoding='utf-8') as f:
        temp = json.load(f)
    for v in temp:
        case_data.append(tuple(v.values()))

    return case_data